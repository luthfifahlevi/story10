from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index, masuk, keluar, daftar
from .apps import HomepageConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User
import time

class UnitTest(TestCase):
    def test_app(self):
        self.assertEqual(HomepageConfig.name, "homepage")

    #tampilan awal
    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Selamat Datang, Calon Teman!")
    def test_index_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    #register
    def test_register_url_is_exist(self):
        response = Client().get('/daftar/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Tak Kenal Maka Tak Sayang, Yuk Isi!")
    def test_register_using_template(self):
        response = Client().get('/daftar/')
        self.assertTemplateUsed(response, 'daftar.html')
    def test_register_func(self):
        found = resolve('/daftar/')
        self.assertEqual(found.func, daftar)

    #login
    def test_login_url_is_exist(self):
        response = Client().get('/masuk/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Login dulu ya :)")
    def test_login_using_template(self):
        response = Client().get('/masuk/')
        self.assertTemplateUsed(response, 'masuk.html')
    def test_login_func(self):
        found = resolve('/masuk/')
        self.assertEqual(found.func, masuk)

    #logout
    def test_logout_func(self):
        found = resolve('/keluar/')
        self.assertEqual(found.func, keluar)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        time.sleep(1)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_register_login_and_logout(self):
        selenium = self.browser
        selenium.get(self.live_server_url+'/')

        button_daftar = selenium.find_element_by_id("daftar")
        button_daftar.click()
        time.sleep(2)

        #mengecek telah di html pendaftaran 
        self.assertIn("Tak Kenal Maka Tak Sayang, Yuk Isi!", selenium.page_source)
        username = selenium.find_element_by_id('id_username')
        password = selenium.find_element_by_id('id_password1')
        confirm_password = selenium.find_element_by_id('id_password2')

        username.send_keys("luthfi")
        password.send_keys("kucingku")
        confirm_password.send_keys("kucingku")
        #submit dan akan menuju login html
        button = selenium.find_element_by_id("setuju")
        button.click()
        time.sleep(3)

        #mengecek telah di login hmtl dan mencoba login
        self.assertIn("Login dulu ya :)", selenium.page_source)
        button = selenium.find_element_by_id("login")
        username = selenium.find_element_by_id("username")
        password = selenium.find_element_by_id("password")
        username.send_keys("luthfi")
        password.send_keys("kucingku")
        button.click()
        time.sleep(3)

        #mengecek telah kembali ke index html dan mencoba logout
        self.assertIn("Selamat Datang, luthfi!", selenium.page_source)
        button_logout = selenium.find_element_by_id("logout")
        button_logout.click()
        time.sleep(3)

        #mengecek telah kembali seperti semula
        self.assertIn("Selamat Datang, Calon Teman!", selenium.page_source)
    
    def test_account_is_not_exist_and_wrong_input(self):
        selenium = self.browser
        selenium.get(self.live_server_url+'/')

        button_login = selenium.find_element_by_id("login")
        button_login.click()

        #mengecek telah di login hmtl dan mencoba login dengan akun tak terdaftar
        self.assertIn("Login dulu ya :)", selenium.page_source)
        button = selenium.find_element_by_id("login")
        username = selenium.find_element_by_id("username")
        password = selenium.find_element_by_id("password")
        username.send_keys("Naruto")
        password.send_keys("akusukahinata")
        button.click()
        time.sleep(3)
        self.assertIn("Username/Password Salah :(", selenium.page_source)

        daftar = selenium.find_element_by_id("daftar")
        daftar.click()
        time.sleep(3)

        #mengecek di halaman pendaftaran dan mendaftar
        self.assertIn("Tak Kenal Maka Tak Sayang, Yuk Isi!", selenium.page_source)
        username = selenium.find_element_by_id('id_username')
        password = selenium.find_element_by_id('id_password1')
        confirm_password = selenium.find_element_by_id('id_password2')

        username.send_keys("luthfi")
        password.send_keys("kucingku")
        confirm_password.send_keys("kucingku")
        #submit dan akan menuju login html
        button = selenium.find_element_by_id("setuju")
        button.click()
        time.sleep(3)

        #mengecek telah di login hmtl dan mencoba login dengan salah input
        self.assertIn("Login dulu ya :)", selenium.page_source)
        button = selenium.find_element_by_id("login")
        username = selenium.find_element_by_id("username")
        password = selenium.find_element_by_id("password")
        username.send_keys("luthfiku") #seharusnya "luthfi"
        password.send_keys("kucingku")
        button.click()
        time.sleep(3)
        self.assertIn("Username/Password Salah :(", selenium.page_source)

    #untuk daftar, telah memiliki validasi sendiri, jadi aman.