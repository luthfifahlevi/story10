from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('daftar/', views.daftar, name='daftar'),
    path('masuk/', views.masuk, name='masuk'),
    path('keluar/', views.keluar, name='keluar'),
]